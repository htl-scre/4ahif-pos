package lombokk;

public class App {
    public static void main(String[] args) {
        var person = new PersonPOJO();
        person.getCountry();
        System.out.println(person);
        var person2 = new Person("fist", "last", null, "Austria", 3L);
        person2.getCountry();
        System.out.println(person2);
        person2.foo(null);
    }
}
