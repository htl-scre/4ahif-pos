package lombokk;

import lombok.experimental.Delegate;

public class Car {

    @Delegate
    private Motor motor;
    private Lights lights;

    public static void main(String[] args) {
        new Car().foo();
    }
}

class Lights {

}

class Motor {

    public void start() {
        System.out.println("starting");
    }

    public  void foo() {}
}
