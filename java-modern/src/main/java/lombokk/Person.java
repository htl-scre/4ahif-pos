package lombokk;


import lombok.*;

import java.io.IOException;
import java.time.LocalDateTime;

@Value
public class Person {
    String firstName;
    @With
    String lastName;
    LocalDateTime dateOfBirth;
    String country;
    @EqualsAndHashCode.Include
    private Long id;

    @SneakyThrows
    public void foo(@NonNull String s) {
        throw new IOException();
    }
}