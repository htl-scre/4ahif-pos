package records;

import java.util.Objects;
import java.util.Optional;

public record Person(String name, int age, String ssn) {

    public Person withName(String name) {
        return new Person(name, this.age, this.ssn);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Person) obj;
        return Objects.equals(this.name, that.name) &&
                this.age == that.age &&
                Objects.equals(this.ssn, that.ssn);
    }

    @Override
    public String toString() {
        return "Person[" +
                "name=" + name + ", " +
                "age=" + age + ", " +
                "ssn=" + ssn + ']';
    }

}
