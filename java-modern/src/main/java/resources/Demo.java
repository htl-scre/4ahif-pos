package resources;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Demo {
    public static void main(String[] args) throws IOException, URISyntaxException {
        // new Demo().getClass()
        var uri = Demo.class.getResource("/text.txt").toURI();
        var path = Path.of(uri);
        System.out.println(Files.readString(path));
    }
}
