package streams;

import java.util.stream.IntStream;

public class Demo {
    public static void main(String[] args) {
        IntStream
                .range(0, 10)
                .map(i -> i + 1)
                .mapToObj(i -> "-".repeat(i));

    }
}
