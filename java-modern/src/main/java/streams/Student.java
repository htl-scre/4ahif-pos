package streams;

import java.util.Optional;
import java.util.OptionalInt;

public class Student {

    private Student buddy;
    private String name;

    public Student(Student buddy) {
        this.buddy = buddy;
    }

    public Optional<Student> getBuddy() {
        return Optional.ofNullable(buddy);
    }

    public static void main(String[] args) {
        Optional<String> optionalName = getX().map(student -> student.name);
        Optional<Optional<Student>> optionalFriend = getX().map(student -> student.getBuddy());
        Optional<Student> optionalFriend_ = getX().flatMap(student -> student.getBuddy());
        var loner = new Student(null);
        var withFriends = new Student(loner);
        if (loner.getBuddy().isPresent())
            System.out.println(loner.getBuddy().get());
        loner.getBuddy().ifPresent(System.out::println);
    }

    public static Optional<Student> getX() {
        return Optional.empty();
    }

}
