package application;

import persistence.StudentRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.StringTemplate.STR;

public class App {
    public static void main(String[] args) throws SQLException {
        var connection = DriverManager.getConnection(
                "jdbc:postgresql://ifpostgres02:5432/scre_customers",
                "unterricht",
                "unterricht"
        );
        try(var statement = connection.createStatement()) {
            var resultSet = statement.executeQuery("""
                    select * from customers
                    where 
                    """);
            while(resultSet.next()) {
                var id = resultSet.getInt("id");
                var name = resultSet.getString("name");
                System.out.println(STR."\{id} \{name}");
            }
        }


        StudentRepository repo = StudentRepository.getInstance(connection);
        var repo2 = StudentRepository.getInstance(connection);
        Map<Class, Object> container = new HashMap<>();
        container.put(String.class, "asd");
        container.put(Connection.class, DriverManager.getConnection(""));
        container.put(StudentRepository.class, new StudentRepository());
    }
}
