package persistence;

import domain.Student;
import lombok.*;

import java.sql.Connection;
import java.util.Optional;

public record StudentRepository(Connection connection) {

    public Student save(Student student) {
        return null;
    }

    public Optional<Student> findById() {
        return null;
    }
}
