package app;

import domain.Actor;
import domain.Address;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;

public class App {
    public static void main(String[] args) {
        var factory = Persistence.createEntityManagerFactory("movies");
        var entityManager = factory.createEntityManager();

        var address = new Address(null, "Vienna",null);
        var actor = new Actor(null, "Bruce", address);
        address.setResident(actor);
        entityManager.getTransaction().begin();
        entityManager.persist(actor);
        entityManager.persist(address);
        entityManager.getTransaction().commit();
    }

    private static void joinFetch(EntityManager entityManager) {
        var query = entityManager.createQuery("""
select actor
from Actor actor
join fetch actor.address
where actor.id = 6
""", Actor.class);
        var actor = query.getSingleResult();

        entityManager.close();
        System.out.println(actor);
    }

    private static void lazyInitError(EntityManager entityManager) {
        //var address = entityManager.merge(new Address(null, "Budapest"));
        var actor = entityManager.find(Actor.class,6);
        entityManager.close();
        System.out.println(actor);
    }
}
