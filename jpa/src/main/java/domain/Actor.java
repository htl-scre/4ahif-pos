package domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "actors")

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor // jpa requires this todo
public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Basic(fetch = FetchType.LAZY) // CAN be fetched lazily
    @Length(max = 255, min = 2)
    private String name;

    @OneToOne
    private Address address;
}
