package persistence;

import domain.Actor;
import jakarta.persistence.EntityManager;

import java.util.Optional;

/*
Mögliche locations zum Verwalten von EntityManagern:
* [leaken persistence context]in einer Klasse, die die main-Methode enthält
* [leaken persistence context]Singleton
* [leaken persistence context]static Variable irgendwo
* in jeder Repository-Methode
* in jedem Repository
 */

public record ActorRepositoryPerMethod() {

    public Optional<Actor> findbyId(int id, EntityManager entityManager) {
        return Optional.ofNullable(entityManager.find(Actor.class, id));
    }


    public Optional<Actor> findbyName(int id, EntityManager entityManager) {
        return entityManager.createQuery("SELECT a FROM Actor a WHERE a.name = :name", Actor.class)
                .setParameter("name", id)
                .getResultList()
                .stream()
                .findFirst();
    }
}
