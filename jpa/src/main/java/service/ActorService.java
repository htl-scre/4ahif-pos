package service;

import domain.Actor;
import jakarta.persistence.EntityManagerFactory;
import persistence.ActorRepository;
import persistence.ActorRepositoryPerMethod;

import java.util.List;
import java.util.Optional;

public record ActorService(EntityManagerFactory factory, ActorRepositoryPerMethod repositoryField) {

    public List<Actor> findbyId(List<Integer> ids) {
        var entityManager = factory.createEntityManager();
        try {
            // ein repo pro Service
            var repository = new ActorRepository(entityManager);
            return ids.parallelStream()
                    .map(repository::findbyId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList();
        } finally {
            entityManager.close();
        }
    }

    public List<Actor> findbyIdNotLiked(List<Integer> ids) {
        var entityManager = factory.createEntityManager();
        try {
            return ids.parallelStream()
                    .map(id -> repositoryField.findbyId(id, entityManager))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList();
        } finally {
            entityManager.close();
        }
    }
}
