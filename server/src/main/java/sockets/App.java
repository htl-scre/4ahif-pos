package sockets;

import lombok.SneakyThrows;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

    @SneakyThrows
    public static void main(String[] args) {
        var executor = Executors.newVirtualThreadPerTaskExecutor();
        executor.submit(new Server());
        for (int i = 0; i < 20; i++)
            executor.submit(new Client(i));
        executor.awaitTermination(1, TimeUnit.DAYS);
    }
}
