package sockets;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.io.PrintWriter;
import java.net.Socket;

@AllArgsConstructor
public class Client implements Runnable{

    private int limit;

    @SneakyThrows
    @Override
    public void run() {
        try (var socket = new Socket("localhost", 8000)) {
            var output = new PrintWriter(socket.getOutputStream()); // autoflush?
            for (int i = 0; i < limit; i++)
                output.println(i);
            output.flush();
        }
    }
}
