package sockets;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable{

    @SneakyThrows
    private static void handleConnection(Socket connection) {
        var input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = input.readLine()) != null)
            System.out.println(line);
    }

    @SneakyThrows
    @Override
    public void run() {
        try (var socket = new ServerSocket(8000)) {
            System.out.println("Accepting connections");
            Socket connection;
            while ((connection = socket.accept()) != null) {
                Socket finalConnection = connection;
                // todo Executors...
                new Thread(() -> handleConnection(finalConnection)).start();
            }
        }
    }
}
