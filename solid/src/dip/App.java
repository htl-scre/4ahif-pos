package dip;

public class App {
    public static void main(String[] args) {
        var dictionary = new EnglishDictionary();
        var spellChecker = new EnglishSpellChecker(dictionary);
        var wordProcessor = new WordProcessor(spellChecker);
    }
}
