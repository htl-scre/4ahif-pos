package dip;

public interface Dictionary {
    void check(String word);
}
