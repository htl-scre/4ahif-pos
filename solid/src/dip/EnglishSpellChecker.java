package dip;

import java.util.ArrayList;

public class EnglishSpellChecker implements SpellChecker {

    private Dictionary dictionary;

    public EnglishSpellChecker(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public void checkSpelling() {
        var words = new ArrayList<String>();
        words.forEach(dictionary::check);
    }
}
