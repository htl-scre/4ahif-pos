package dip;

public interface SpellChecker {
    void checkSpelling();
}
