package dip;

public class WordProcessor {

    // new is glue
    private SpellChecker spellChecker;

    public WordProcessor(SpellChecker spellChecker) {
        this.spellChecker = spellChecker;
    }

    public void checkSpelling() {
        spellChecker.checkSpelling();
    }

    public void doWordStuff() {
    }

    ;
}
