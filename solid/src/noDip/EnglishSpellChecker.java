package noDip;

import java.util.ArrayList;

public class EnglishSpellChecker {

    private EnglishDictionary dictionary = new EnglishDictionary();

    public void checkSpelling() {
        var words = new ArrayList<String>();
        words.forEach(dictionary::check);
    }
}
