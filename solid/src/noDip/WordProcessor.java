package noDip;

public class WordProcessor {

    private EnglishSpellChecker spellChecker = new EnglishSpellChecker();

    public void checkSpelling() {
        spellChecker.checkSpelling();
    }

    public void doWordStuff() {};
}
