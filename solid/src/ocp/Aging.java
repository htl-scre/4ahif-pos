package ocp;

import java.time.LocalDate;

public interface Aging {

    LocalDate getDateOfBirth();
}
