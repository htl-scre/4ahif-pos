package ocp;

import java.time.LocalDate;

import static java.lang.StringTemplate.STR;

public class Person implements Aging{

    private Personalia personalia;

    public Person(Personalia personalia) {
    }

    public String getName() {
        return personalia.name();
    }


    public String getAddress() {
        // return `${age}, ${name}`
        return STR."Street: \{ personalia.address().street()}, \{}";
    }

    @Override
    public LocalDate getDateOfBirth() {
        return personalia.dateOfBirth();
    }
}
