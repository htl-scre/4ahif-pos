package ocp;

import java.time.LocalDate;

public record Personalia(String name,
                         Address address,
                         LocalDate dateOfBirth
) {
}
