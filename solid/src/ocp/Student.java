package ocp;

import java.time.LocalDate;

public class Student implements Aging {
    private int number;
    private Personalia personalia;
    public String address;

    public Student(int number, Personalia personalia) {
        this.number = number;
        this.personalia = personalia;
        address = personalia.address().toString();
    }

    @Override
    public LocalDate getDateOfBirth() {
        return personalia.dateOfBirth();
    }
}
